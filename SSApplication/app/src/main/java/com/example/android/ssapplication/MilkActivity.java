package com.example.android.ssapplication;

import android.os.Bundle;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MilkActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    TvShowAdapter tvShowAdapter;
    ArrayList<TvShow> tvShows;
    RecyclerView.LayoutManager layoutManager ;

    public static final String[] TvShows = {"COW 1","COW 2","COW 3","COW 4","COW 5","COW 6","COW 7","COW 8","COW 9","COW 10","COW 11","COW 12","COW 13","COW 14","COW 15","COW 16","COW 17","COW 18","COW 19","COW 20","COW 21"};
    public static final int[] TvShowImgs = {R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background,R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background,R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background,R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background, R.drawable.cow1_background,R.drawable.cow1_background};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_milk);
        tvShows = new ArrayList<>();
        layoutManager = new GridLayoutManager(this,2);
        for (int i = 0; i < TvShows.length; i++) {
            TvShow tvShow = new TvShow();

            tvShow.setTvshow(TvShows[i]);
            tvShow.setImgTvshow(TvShowImgs[i]);

            tvShows.add(tvShow);
        }
        tvShowAdapter = new TvShowAdapter(tvShows);

        recyclerView = (RecyclerView) findViewById(R.id.TvShows);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(tvShowAdapter);
    }
}
